# Changelog

All notable changes to this project will be documented in this file. In order to maintain this file through automation, all commits to this repository must adhere to the guidelines laid out by [Conventional Commits](https://conventionalcommits.org) and the [Megabyte Labs Commit Guide](https://megabyte.space/docs/contributing/commits).

## [2.9.3](https://gitlab.com/megabyte-labs/docker/ci-pipeline/docker-test/compare/v2.9.2...v2.9.3) (2022-08-25)





Grab this version by running:


```shell
docker pull megabytelabs/docker-test:2.9.2
```
